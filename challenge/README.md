# Welcome to the SDN challenge!

The goal of this challenge is to speed your hands-on capabilities with our SDN lab. If you are able to solve this challenge, you will gain extra extra points at the final exam! (To be announced during classes)

## Getting started

To successfully complete this assignment, you are required to develop the challengeApp.py Python program. The objective is to establish an end-to-end connection between two hosts within an SDN openflow-based network.

- [ ] Start Virtualbox and run the NA-2023-2024 virtual machine
- [ ] Start Bitvise and open 2 different terminals
- [ ] Move into the challenge folder on each terminal, see figure below 

```
cd network_automation/code-repository/challenge
```

![ALT](/images/fig5.png "Terminals-1")

## How to run the challenge

Run the challenge as follows.
- [ ] Code your app by modifying the challengeApp.py
- [ ] Run the tester app in the first terminal as follows:
```
sudo python3 mininet_test.pyc
```
- [ ] Test your app by running with the following shell command in the second terminal, see figure below : 
```
ryu-manager --observe-links challengeApp.py
```

![ALT](/images/fig6.png "Terminals-2")

## Deliver your solution

If you successfully solve the challenge, you will receive a message with instructions on how to submit your solution.


