# Getting started

To facilitate the initiation of your Network Automation project, we have compiled a step-by-step guide for setting up your laptop/PC.

## Download and install the software tools
- [ ] [Virtualbox](https://www.virtualbox.org/wiki/Downloads)
- [ ] [Bitvise SSH Client](https://www.bitvise.com/download-area)


## Laptop/PC setup
- [ ] Download the Virtual Machine (VM) of the course [NA-2023-2024-students.ova](https://www.dropbox.com/scl/fi/y186x7x636a4mip6y5884/NA-lab-2023-2024-students.ova?rlkey=utcq6ryklhyxe2za0tlru0qed&dl=0)
- [ ] Import the NA-2023-2024-students.ova into VirtualBox (File->Import Applience) and keep the default settings, see figure below.

![ALT](/images/fig2-3.png "Virtualbox")

- [ ] Start the VM, see figure below.

![ALT](/images/fig4.png "Start VM")

- [ ] Username and Password to access the VM are the following: 
- - [ ] User: ubuntu 
- - [ ] Password: ubuntu
- [ ] Bitvise setup to access with SSH to the VM (see figure below):
- - [ ] Host: localhost
- - [ ] Port: 2223  
- - [ ] Username: ubuntu
- - [ ] Initial method: password
- - [ ] Password: ubuntu
- [ ] Click on "Login" and "Accept and Save" the new host key

![ALT](/images/fig1.png "Bitvise")

## Gitlab repository
- [ ] [Register](https://gitlab.com/users/sign_in#register-pane) to gitlab. After the registration you will have a username and a password
- [ ] Add your Git username and set your email into the VM: 
```
git config --global user.name "username"
git config --global user.email "your_email@example.com"
```

- [ ] Move into the folder "network-automation/" and clone the code template project
```
cd network-automation
git clone https://gitlab.com/network-automation-code-repository/2023-2024/code-repository.git
```

# Apple Silicon Guide

To run this virtual machine on ARM architecture, a different approach is required. A container equipped with all the necessary tools is provided.

## Download and install the software tools
- [ ] [Docker](https://www.docker.com)
- [ ] [Visual Studio Code](https://code.visualstudio.com)
- [ ] [Dev Containers Plugin for VSCode](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers)

## Environment setup
- [ ] Launch Docker
- [ ] Download the `docker-sdn` folder from this repository.
- [ ] Copy this folder to the home directory `(~/)`.
- [ ] In the terminal, and within the home directory, run:
```
docker build -t sdn docker-sdn
```
- [ ] Navigate to the `docker-sdn` folder:
```
cd docker-sdn
```
- [ ] Start the container with the following command:
```
docker-compose run --rm sdn
```

## Container execution
There is no need to repeat all the steps above every time you start the container. Simply navigate to the folder and execute `docker-compose run --rm sdn`. However, to streamline this process, you can define an alias in `.zshrc`, such as:
```
alias docker-sdn="cd ~/docker-sdn && docker-compose run --rm sdn"
```
Due to some limitations introduced by Docker, it is essential to use Visual Studio Code. After installing it and starting the container, you can connect to it using the plugin. Click on the symbol in the bottom-left corner of VSCode, select 'Attach to a Running Container,' and choose the `docker-sdn` container. This will provide you with an interface inside the container.

### Notes
- The system may crash when executing unusual commands, due to resource limitations.
- Unlike the Ubuntu VM, this container must use Docker's `host` networking mode to prevent Mininet from crashing.
- You can easily create terminal sessions inside VSCode, eliminating the need for an SSH client.
- The `docker-compose.yml` file declares a shared folder, which can be found at `~/docker-sdn/shared/` on the host and `~/desktop/` in the container. This folder allows for easy file sharing between the host and the container.